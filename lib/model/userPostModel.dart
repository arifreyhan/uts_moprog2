import 'dart:convert';


List userModelFromJson(String str) => List.from(json.decode(str));

String userpostModelToJson(UserpostModel data) => json.encode(data.toJson());

class UserpostModel {
  UserpostModel({
    this.nama_mobil,
    this.deskripsi_mobil,
    this.harga_mobil,
    this.url_gambar,
   
  });

  String nama_mobil;
  String deskripsi_mobil;
  String harga_mobil;
  String url_gambar;

  

  Map toJson() => {
        "nama_mobil": nama_mobil,
        "deskripsi_mobil": deskripsi_mobil,
        "harga_mobil": harga_mobil,
        "url_gambar": url_gambar,
      };
}
