// import 'package:flutter/material.dart';
// import 'package:showroom_mobil/screen/home_page.dart';

// import 'get_model.dart';

// class Biodata extends StatefulWidget {
//   Biodata({Key key}) : super(key: key);

//   @override
//   _BiodataState createState() => _BiodataState();
// }

// class _BiodataState extends State<Biodata> {
//   MyHomePage myHomePage;
//   UserGet userGet;

// @override
// void initState() { 
//   super.initState();
//   UserGet.connectToApiUser('3').then((value) {
//     setState(() {
//       userGet = value;
//     });
//   });
// }

//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//        child: Scaffold(
//          body: Center(
//            child: Container(
//              height: MediaQuery.of(context).size.height,
//              color: Colors.blue[100],
//              alignment: Alignment.center,
//              padding: EdgeInsets.all(30),
//              child: Column(
//                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                children: [
//                  Text(
//                    "WELCOME",
//                    style: TextStyle(fontSize: 48, color: Color(0xff0563BA)), 
//                  ),
//                  Container(
//                    child: Column(
//                   children: [
//                     ClipRRect(
//                       borderRadius: BorderRadius.circular(70.0),
//                       child: Image.network(
//                         userGet.avatar,
//                         cacheHeight: 128,
//                         cacheWidth: 128,
//                         fit: BoxFit.cover,
//                       ),
//                     ),
//                  Padding(
//                    padding: const EdgeInsets.all(8.0),
//                    child: Text(
//                      userGet.firstname + " " + userGet.lastname,
//                      style: 
//                           TextStyle(fontSize: 24, color: Color(0xff0563BA)),
//                    ),
//                  ),
//                  Text(
//                    userGet.email,
//                    style: TextStyle(fontSize: 18, color: Colors.black),
//                  ) 
//                 ]   
//                )
//               ) 
//            ],)
//           ),
//          )
//         )
//     );
//   }
// }