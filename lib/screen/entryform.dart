import 'package:flutter/material.dart';
import 'package:showroom_mobil/model/userPostModel.dart';
import 'package:showroom_mobil/model/userViewModel.dart';
import 'package:showroom_mobil/screen/home_page.dart';
class Entryform extends StatefulWidget {
  Entryform({Key key}) : super(key: key);

  @override
  _EntryformState createState() => _EntryformState();
}


class _EntryformState extends State<Entryform> {
final TextEditingController _nama = new TextEditingController();
final TextEditingController _gambar = new TextEditingController();
final TextEditingController _harga = new TextEditingController();
  final TextEditingController _deskripsi = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(child: Container(
        margin: EdgeInsets.all(20),
        child: Column(children: [
         Container(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 3, bottom: 3),
                          child: TextFormField(
                            controller: _nama,
                            decoration: InputDecoration(
                                labelText: "Nama Mobil",
                                labelStyle: TextStyle(color: Colors.grey)),
                            validator: (input) => !input.contains("@")
                                ? "Please enter a valid email"
                                : null,
                            onSaved: (input) => print(input),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 13,
                      ),
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 3, bottom: 3),
                          child: TextFormField(
                            controller: _gambar,
                            decoration: InputDecoration(
                                labelText: "Alamat Gambar",
                                labelStyle: TextStyle(color: Colors.grey)),
                            validator: (input) => !input.contains("@")
                                ? "Please enter a valid email"
                                : null,
                            onSaved: (input) => print(input),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 13,
                      ),
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 3, bottom: 3),
                          child: TextFormField(
                            controller: _harga,
                            decoration: InputDecoration(
                                labelText: "Harga",
                                labelStyle: TextStyle(color: Colors.grey)),
                            validator: (input) => !input.contains("@")
                                ? "Please enter a valid email"
                                : null,
                            onSaved: (input) => print(input),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 13,
                      ),
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 3, bottom: 3),
                          child: TextFormField(
                            controller: _deskripsi,
                            decoration: InputDecoration(
                                labelText: "Deskripsi",
                                labelStyle: TextStyle(color: Colors.grey)),
                            validator: (input) => !input.contains("@")
                                ? "Please enter a valid email"
                                : null,
                            onSaved: (input) => print(input),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 13,
                      ),
                      Container(
                        child: InkWell(
                          onTap: (){
                             UserpostModel commRequest = UserpostModel();
                              commRequest.nama_mobil = _nama.text;
                              commRequest.deskripsi_mobil = _deskripsi.text;
                              commRequest.harga_mobil = _harga.text;
                              commRequest.url_gambar = _gambar.text;

                      UserViewModel()
                        .postUser(userpostModelToJson(commRequest))
                        .then((value) => print('Berhasil'));
                            showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: Text('Pemberitahuan'),
                                  content: Container(
                                    child: Text('Oke menambahkan' +
                                        _nama.text +
                                        " " +
                                        _deskripsi.text +
                                        " " +
                                        _harga.text +
                                        " " +
                                        _gambar.text ),
                                  ),
                                  actions: [
                                    TextButton(
                                        onPressed: () {
                                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => MyHomePage()));
                                        },
                                        child: Text('Tutup'))
                                  ],
                                );
                              });
                          },
                          child: Container(
                            margin: EdgeInsets.all(0),
                            width: double.infinity,
                            height: 70,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: Colors.blueAccent),
                            child: Center(child: Text("Tambah", style: TextStyle(color: Colors.white),)),
                          ),
                        ),
                      ),
      ],),)),
    );
  }
}